import requests
import sys
import re

'''
This script retrieve data from haveibeenpawned.com about all websites that was breach and what type
of data has been lead to internet. To use this script you add specific sites as arguments that you are interest 
or retrieve all hacked sites by just ruining the script
'''
url_for_all = 'https://haveibeenpwned.com/api/v3/breaches'
url_for_one = 'https://haveibeenpwned.com/api/v3/breach/{site_name}'
requested_sites = sys.argv[1:]


def remove_html_tags(text):
    '''
    Remove html tags from given text
    :param text: string with html tags
    :return: formatted string
    '''
    clean = re.compile('<.*?>')
    return re.sub(clean, '', text)


def request_data(sites):
    '''
    Send request to API and return response.
    :param sites: list of sites that are given as parameters to this script
    :return: json object
    '''
    data = []
    if sites:
        for site in sites:
            request_url = url_for_one.format(site_name=site)
            response = requests.get(request_url)
            data.append(response.json())
    else:
        response = requests.get(url_for_all)
        data = response.json()

    return data
    # TODO add error handling


def get_data_from_res(response):
    '''
    Function grab the given data and format a string
    :param response: json object given wit the response from API
    :return: formatted string
    '''
    for dictionary in response:
        name, domain, breach_date, description, data_classes_list = dictionary['Name'], dictionary['Domain'], \
                                                                    dictionary['BreachDate'], \
                                                                    remove_html_tags(dictionary['Description']), \
                                                                    dictionary['DataClasses']
        data_classes_string = ', '.join(data_classes_list)
        yield f'Site Name: {name}, Domain: {domain}, Date: {breach_date},\n\t{description} \nTypes of data stolen: {data_classes_string}\n\n'
    # TODO add response if site was not breched


def print_all_data(formated_string):
    '''
    :param formated_string: string that is formatted by get_data_from_res()
    '''
    for each in formated_string:
        print(each)


# print_all_data(get_data_from_res(request_data(requested_sites)))
def main():
    print_all_data(get_data_from_res(request_data(requested_sites)))


if __name__ == '__main__':
    main()
