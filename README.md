# PasswordChecker


The project aims to create a script that will be able to check whether our password has leaked (has been taken over by unauthorized persons) 
using the API provided by https://haveibeenpwned.com/ 

Using the API is completely secure due to the use of hashing (SHA1) and k-anonymity.  

To use the script, you must enter the password you are looking for as a parameter when starting. The parameters can specify multiple passwords separated by a single space for an example:
`checkmypass.py password1 password2`  

In response, we will receive information about whether our password is contained in the password databases available on the network  

`python checkmypass.py password1 password2 siojfbnsikdjhfbsdif`  
`"password1" was found 2413945 times... you should probably change your password`  
`"password2" was found 185178 times... you should probably change your password`  
`"siojfbnsikdjhfbsdif" was not found. You good to go`