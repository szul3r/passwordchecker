import requests
import hashlib
import sys
'''
To send the password, I use SHA1 and k-anonymity, so in fact we only send the first 5 characters of our hash password.
In response, we receive all passwords (hashed) that have leaked, matching our request, so we can check on our side
whether the password leaked or not. This will make us sure that our password that was sent will not be taken 
by any unauthorized person.

Why SHA1 because api use it.
'''


def request_api_data(query_char):
    """
    Retrieve data from API. Remember to pass first 5 chars from hashed password (k-anonymity) in return we
    get tails of hashed data that was given in query (from char 5 to end)
    :param query_char: hashed (SHA1) password
    :raise RuntimeError: if is unable to connect to API (response is different than 200)
    :return: class 'requests.models.Response' (iterable object) contains all matching data
    """
    url = f'https://api.pwnedpasswords.com/range/{query_char}'
    res = requests.get(url)
    if res.status_code != 200:
        raise RuntimeError(f'Error fetching: {res.status_code}, check API and try again')
    else:
        return res

def get_password_leaks_count(hashes, hash_to_check):
    """
    Compare received data from API and our hashed password (tails)
    :param hashes: Response object which contains a server’s response to an HTTP request
    :param hash_to_check: tail of our hashed password
    :return: number of times our password has been leaked. 0 if not found
    """
    hashes = (line.split(':') for line in hashes.text.splitlines())
    for h, count in hashes:
        if h == hash_to_check:
            return count
    return 0


def pawned_api_check(password):
    """
    Function hashes and prepare given password (separate first 5 chars from rest).
    :param password: String containing our password
    :return: number of times our password has been leaked
    """
    sha1passwd = hashlib.sha1(password.encode('utf-8')).hexdigest().upper()
    first5_char, tail = sha1passwd[:5], sha1passwd[5:]
    try:
        response = request_api_data(first5_char)
    except RuntimeError as e:
        sys.exit(print(e))
    else:
        return get_password_leaks_count(response, tail)

def main(args):
    for password in args:
        count = pawned_api_check(password)
        if count:
            print(f'"{password}" was found {count} times... you should probably change your password')
        else:
            print(f'"{password}" was not found. You good to go')

    return('done')
if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
